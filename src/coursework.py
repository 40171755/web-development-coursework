import ConfigParser, sqlite3 as lite, cgi

from flask import Flask, render_template, request, url_for, g, redirect

app = Flask(__name__)

def connect():
    return lite.connect("database.db")

@app.route('/')
def route():
    return redirect('/index/')

@app.route('/index/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
      q = request.form['name']
    return render_template('index.html')

@app.route('/search/', methods=["POST", "GET"])
def search():
    check = request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT * FROM collection")
    else:
      cur = g.db.execute ("SELECT * FROM collection where (artist like ?) OR (song like ?) OR (album like ?) OR (genre like ?) ORDER BY artist",
      ("%" + check + "%", "%" + check + "%", "%" + check + "%", "%" + check + "%" ))
    search = [dict(artist=row[0], song=row[1], album=row[2], genre=row[3],
    rating=row[4], release=row[5]) for row in cur.fetchall()]
    return render_template('search.html', check=check, search=search)

@app.route('/artists/', methods=["POST", "GET"])
def artists():
    check = request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT DISTINCT artist FROM collection ORDER BY artist")
    else:
      cur = g.db.execute ("SELECT DISTINCT artist FROM collection where artist like ? ORDER BY artist",
      ("%" + check + "%", ))
    artist = [dict(artist=row[0]) for row in cur.fetchall()]
    return render_template('artist.html', check=check, artist=artist)

@app.route('/songs/')
def song():
    check = request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT * FROM collection ORDER BY artist, song")
    else:
      cur = g.db.execute ("SELECT * FROM collection where song like ? ORDER BY artist, song",
      ("%" + check + "%", ))
    song = [dict(artist=row[0], song=row[1], album=row[2], genre=row[3],
    rating=row[4], release=row[5]) for row in cur.fetchall()]
    return render_template('songs.html', check=check, song=song)

@app.route('/genres/')
def genres():
    check= request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT * FROM collection ORDER BY genre, artist, song")
    else:
      cur = g.db.execute ("SELECT * FROM collection where genre like ? ORDER BY genre, artist, song",
      ("%" + check + "%", ))
    genre = [dict(artist=row[0], song=row[1], genre=row[3]) for row in cur.fetchall()]
    return render_template('genres.html', check=check, genre=genre)

@app.route('/ratings/')
def ratings():
    g.db = connect()
    cur = g.db.execute ("SELECT * FROM collection ORDER BY rating DESC, artist, song")
    rating = [dict(artist=row[0], song=row[1], rating=row[4]) for row in cur.fetchall()]
    return render_template('ratings.html', rating=rating)

@app.route('/add/', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
      artist = request.form['artist']
      song = request.form['song']
      album = request.form['album']
      genre = request.form['genre']
      rating = request.form['rating']
      release = request.form['release']
      if (artist != '' and song != '' and album != '' and genre != '' and release != ''):
        g.db = connect()
        cur = g.db.execute ("INSERT INTO collection(artist, song, album, genre, rating, release) VALUES (?, ?, ?, ?, ?, ?)", (artist, song, album, genre, rating, release))
        g.db.commit()
      else:
        print "Error, must not leave blank spaces"
    return render_template('add.html')

@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html'), 404

@app.route('/config/')
def config():
  str = []
  str.append('Debug:'+app.config['DEBUG'])
  str.append('port:'+app.config['port'])
  str.append('url:'+app.config['url'])
  str.append('ip_address:'+app.config['ip_address'])
  return '\t'.join(str)

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = "etc/config.cfg"
    config.read(config_location)

    app.config['DEBUG'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
  except:
    print "Could not read configs from: ", config_location

if __name__ == '__main__':
  init(app)
  app.run(
    host=app.config['ip_address'],
    port=int(app.config['port']))
